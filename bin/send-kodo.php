<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$notifier = $container->getByType('App\Model\Notifier');
$not_paid = $people->getNotPaid(14);

if (empty($not_paid)) {
    die();
}

$latte = new Latte\Engine;
$body = $latte->renderToString(__DIR__."/templates/kodo.latte",
		array("not_paid" => $not_paid));

$notifier->send("Upozorneni na cleny s prispevkem 14 a vice dnu po splatnosti", $body, "koordinatori@pirati.cz");

