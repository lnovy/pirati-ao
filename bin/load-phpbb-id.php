<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');

$all = $people->getMembersWithoutPhpbbId();

foreach ($all as $person) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, 'https://graph.pirati.cz/user/'.$person['username']);
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result);
    if (empty($obj) || $obj->rank != "member") {
            continue;
    }
    $people->setPhpbbId($person['id'],$obj->user_id);
}

