<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$notifier = $container->getByType('App\Model\Notifier');


$all = $people->getMembers();

$not_found = array();

foreach ($all as $person) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, 'https://graph.pirati.cz/user/'.$person['username']);
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result);
    if (empty($obj) || $obj->rank != 'member') {
        $not_found[] = $person['username'];
        continue;
    }
}
if (!empty($not_found)) {
        $notifier->send("Členové chybí ve fóru",
                        "Následující členové chybí ve fóru:<br />".join("<br />",$not_found));
}

