<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$notifier = $container->getByType('App\Model\Notifier');

$people->unsetNotificationSent();
$person = $people->getOneForNotification();

if (empty($person)) die();

$latte = new Latte\Engine;
$body = $latte->renderToString(__DIR__."/templates/notification.latte", array("person" => $person));

$notifier->send("Pirati - blizi se splatnost clenskeho prispevku",$body, strtolower($person->username)."@pirati.cz", false);

$people->setNotificationSent($person->id);
