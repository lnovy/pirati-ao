<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$notifier = $container->getByType('App\Model\Notifier');
$people->unsetReminderSent();
$person = $people->getOneForReminder();

if (empty($person)) die();

$person->pay = clone $person->date_next;
$person->pay->add(new DateInterval('P1M'));

$latte = new Latte\Engine;
$body = $latte->renderToString(__DIR__."/templates/reminder.latte", array("person" => $person));
$notifier->send("Pirati - neuhrazeny clensky prispevek", $body, strtolower($person->username)."@pirati.cz", false);

$people->setReminderSent($person->id);

