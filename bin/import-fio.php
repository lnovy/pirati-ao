<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$payments = $container->getByType('App\Model\Payments');
$fio = $container->getByType('App\Model\Fio');

$fio->importPayments(new \Nette\Utils\DateTime());

