<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$notifier = $container->getByType('App\Model\Notifier');

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://graph.pirati.cz/users');
$result = curl_exec($ch);
curl_close($ch);
$obj = json_decode($result);

if (empty($obj)) {
    $notifier->send("Nepodařilo se kontaktovat GAPI.", "Nepodařilo se kontaktovat GAPI.");
    die();
}
$not_found = array();
foreach ($obj as $person) {
	if ($person->rank!='member') {continue;}
	$p = $people->getByUsername($person->username);
	if (empty($p) || $p->status != 'clen') {
		$not_found[] = $person->username;
	}
}
if (!empty($not_found)) {
	$notifier->send("Členové chybí v pomocné evidenci",
			"Následující členové chybí v pomocné evidenci:<br />".join("<br />",$not_found));
}
