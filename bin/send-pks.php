<?php

$container = require __DIR__ . '/../app/bootstrap.php';
$people = $container->getByType('App\Model\People');
$areas = $container->getByType('App\Model\Areas');
$notifier = $container->getByType('App\Model\Notifier');


$regions = $areas->getAll();

foreach ($regions as $region) {
    $not_paid = $people->getNotPaidRegion($region->id, 14);
    if (empty($not_paid)) {
        continue;
    }
    
    $latte = new Latte\Engine;
    $body = $latte->renderToString(__DIR__."/templates/pks.latte",
                    array("not_paid" => $not_paid));

    $notifier->send("Upozorneni na cleny s prispevkem 14 a vice dnu po splatnosti", $body, $region->email);

}

