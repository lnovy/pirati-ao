<?php

namespace App\Model;

class Fio extends \Nette\Object {

    private $token;
    private $database;

    function __construct($token, $database) {
        $this->token = $token;
        $this->database = $database;
    }

    public function importPayments($date = null) {
        if (empty($date)) {
            $date = new \Nette\Utils\DateTime;
        }
        $date_to = $date->format("Y-m-d");
        $date->sub(new \DateInterval('P5D'));
        $date_from = $date->format("Y-m-d");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://www.fio.cz/ib_api/rest/periods/' . $this->token . '/' . $date_from . '/' . $date_to . '/transactions.json');
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        foreach ($obj->accountStatement->transactionList->transaction as $row) {
            $payment = array();
            $payment['id'] = $row->column22->value;
            $payment['date'] = new \DateTime($row->column0->value);
            $payment['amount'] = $row->column1->value;
            $payment['currency'] = $row->column14->value;
            if ($payment['amount'] <= 0 || $payment['currency'] != 'CZK') {
                continue;
            }
            if (!empty($row->column4)) {
                $payment['ks'] = $row->column4->value;
            }
            if (!empty($row->column5)) {
                $payment['vs'] = $row->column5->value;
            }
            if (!empty($row->column6)) {
                $payment['ss'] = $row->column6->value;
            }
            if (!empty($row->column7)) {
                $payment['identity'] = $row->column7->value;
            }
            if (!empty($row->column16)) {
                $payment['message'] = $row->column16->value;
            }

            $payment_in_db = $this->database->fetch("SELECT * "
                    . "FROM payments "
                    . "where id=?;", $payment['id']);
            if (empty($payment_in_db)) {
                $this->database->query("insert into payments ", $payment);
            }
        }
    }

}
