<?php

namespace App\Model;

use Nette;


/**
 * Areas management.
 */
class Areas extends Nette\Object 
{

    	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}
        
        public function getAll() {
		return $this->database->fetchAll("SELECT * FROM areas;");
	}
    

}