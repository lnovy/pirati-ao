<?php

namespace App\Model;

class Payments extends \Nette\Object {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function update() {
        $this->database->query("update people p
			join payments pa on p.phpbb_id=pa.vs
			set p.date_paid=pa.date,
				p.date_next=date_add(p.date_next,interval 1 year),
				p.notification_sent=null,
				p.reminder_sent=null,
				pa.date_used=now(),
				pa.person_id=pa.vs
			where pa.ss like '13%'
			and pa.amount>=200
			and pa.amount<=49999
			and pa.date_used is null
			and pa.person_id is null
			and year(date_next)>=2016;");
    }

}
