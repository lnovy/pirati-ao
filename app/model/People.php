<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class People extends Nette\Object 
{
	const
		TABLE_NAME = 'people',
		COLUMN_ID = 'id';

	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function get($id) {
		return $this->database->fetch("SELECT * FROM people WHERE id=?;",$id);
	}

	public function getByUsername($username) {
		return $this->database->fetch("SELECT * FROM people WHERE username=?;",$username);
	}

	public function getAll() {
		return $this->database->fetchAll("SELECT * FROM people;");
	}
	
	public function getMembers() {
		return $this->database->fetchAll("SELECT * FROM people WHERE status='clen';");
	}

	/**
	 * Get person without phpbb_id.
	 * @return array
	 */
	public function getOneWithoutPhpbbId()
	{
		return $this->database->fetch("SELECT * FROM people WHERE phpbb_id IS NULL order by rand();");
	}

        /**
	 * Get people without phpbb_id.
	 * @return array
	 */
	public function getMembersWithoutPhpbbId()
	{
		return $this->database->fetchAll("SELECT * "
                        . "FROM people "
                        . "WHERE phpbb_id IS NULL "
						. "and date_end IS NULL "
                        . "and status='clen';");
	}

        
	public function setPhpbbId($id, $phpbb_id) {
		$this->database->query("UPDATE people SET phpbb_id=? WHERE id=?;",$phpbb_id, $id);
	}
	
	public function getOneForNotification() {
		return $this->database->fetch("select p.*,a.name as area_name "
				. "from people p "
				. "join areas a ON (p.area_id = a.id) "
				. "where p.date_end is null "
				. "and p.status='clen' "
				. "and p.notification_sent is null "
				. "and p.phpbb_id is not null "
				. "and p.date_next>now() "
				. "and p.date_next<date_add(now(),interval 2 week)"
				. "order by rand();");
	}

	public function getOneForReminder() {
		return $this->database->fetch("select p.*,a.name as area_name "
				. "from people p "
				. "join areas a ON (p.area_id = a.id) "
				. "where p.date_end is null "
				. "and p.status='clen' "
				. "and p.notification_sent is not null "
				. "and p.reminder_sent is null "
				. "and p.phpbb_id is not null "
				. "and p.date_next<date_sub(now(),interval 1 week) "
				. "and p.date_next>date_sub(now(),interval 1 month) "
				. "order by rand();");
	}
	
	public function setNotificationSent($id) {
		$this->database->query("UPDATE people "
				. "SET notification_sent=now() "
				. "where id=?;",$id);
	}
 
	public function unsetNotificationSent() {
            $this->database->query("UPDATE people "
                    . "SET notification_sent=null "
                    . "WHERE date_next>now() and notification_sent<date_sub(now(), interval 1 month) and status='clen';");
        }

	public function setReminderSent($id) {
		$this->database->query("UPDATE people "
				. "SET reminder_sent=now() "
				. "where id=?;",$id);
	}

	public function unsetReminderSent() {
            $this->database->query("UPDATE people "
                    . "SET reminder_sent=null "
                    . "where date_next>now() and status='clen';");
        }

	public function getNotPaid($interval=0) {
		return $this->database->fetchAll("select p.*,a.name as area_name 
				from people p 
				join areas a ON (p.area_id = a.id) 
				and status='clen' 
				and p.date_next<date_sub(now() , interval ? day)
				order by p.date_next;", $interval);
	}
        
	public function getNotPaidRegion($region, $interval=0) {
		return $this->database->fetchAll("
                    select p.*,a.name as area_name, a.email as area_email
				from people p 
				join areas a ON (p.area_id = a.id) 
				and status='clen' 
                                and a.id = ?
                                and p.date_next<date_sub(now() , interval ? day)
				order by p.date_next;", $region, $interval);
	}
        
}



class DuplicatePhpbbIdException extends \Exception
{}
