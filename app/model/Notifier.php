<?php

namespace App\Model;

class Notifier extends \Nette\Object {

    private $from;
    private $kodo;
    private $bcc;
    private $mailer;

    public function __construct($from, $kodo, $bcc, $mailer) {
        $this->from = $from;
        $this->kodo = $kodo;
        $this->bcc = $bcc;
        $this->mailer = $mailer;
    }

    public function send($subject, $message, $to = null, $html = true) {
        $mail = new \Nette\Mail\Message();
        $mail->setFrom($this->from);
        if (!empty($to)) {
            $mail->addTo($to);
        }
        if (is_array($this->bcc)) {
            foreach ($this->bcc as $address) {
                $mail->addBcc($address);
            }
        } elseif (!empty($this->bcc)) {
            $mail->addBcc($this->bcc);
        }
        $mail->setSubject($subject);
        if ($html) {
            $mail->setHtmlBody($message);
        } else {
            $mail->setBody($message);
        }

        $this->mailer->send($mail);
    }

    public function sendKodo($subject, $message) {
        $this->send($subject, $message, $this->kodo);
    }

}
